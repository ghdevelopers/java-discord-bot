package com.br.GHDeveloper.model;

import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class Command {

	private String[] args;
	private MessageReceivedEvent event;
	
	public String[] getArgs() {
		return args;
	}
	public void setArgs(String[] args) {
		this.args = args;
	}
	public MessageReceivedEvent getEvent() {
		return event;
	}
	public void setEvent(MessageReceivedEvent event) {
		this.event = event;
	}
	
}
