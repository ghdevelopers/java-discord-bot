package com.br.GHDeveloper.listeners;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import org.jetbrains.annotations.NotNull;

import com.br.GHDeveloper.configuration.Configuration;
import com.br.GHDeveloper.functions.teste;
import com.br.GHDeveloper.model.Command;

import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class EventListener extends ListenerAdapter {

	@Override
	public void onMessageReceived(@NotNull MessageReceivedEvent event) {
		String message = event.getMessage().getContentRaw();

		if(message.startsWith(Configuration.funcCaller)) {
			String[] content = message.substring(1,message.length()).split(" "); 

			try {
				
				Class<?> c = Class.forName("com.br.GHDeveloper.functions."+content[0]);
				Method method = c.getDeclaredMethod("execute", Command.class);
				
				
				
				
				Command temp = new Command();
				temp.setEvent(event);
				temp.setArgs(content);
				
				
				Object test = method.invoke(c.newInstance(), temp);
				
			} catch(ClassNotFoundException e) {
				System.err.println("ClassNotFoundException -> "+e.getMessage());
				event.getChannel().sendMessage("ESSE COMANDO NÃO EXISTE, CAVALO.");
			} catch(NoSuchMethodException e) {
				System.err.println("NoSuchMethodException -> "+e.getMessage());
			} catch(SecurityException e) {
				System.err.println("SecurityException -> "+e.getMessage());
			} catch (IllegalAccessException e) {
				System.err.println("IllegalAccessException -> "+e.getMessage());
			} catch (IllegalArgumentException e) {
				System.err.println("IllegalArgumentException -> "+e.getMessage());
			} catch (InvocationTargetException e) {
				System.err.println("InvocationTargetException -> "+e.getMessage());
			}catch ( InstantiationException e) {
				System.err.println("InstantiationException -> "+e.getMessage());
			}
			
			
		}

	}

}
